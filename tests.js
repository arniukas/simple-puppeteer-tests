const loginT = require('./tests/User/LoginTest');
const chekoutT = require('./tests/Checkout/ChekoutTest');
const sizechartT = require('./tests/Other/SizeChartTest');
const registrationT = require('./tests/User/RegistrationTest');
const newsletterT = require('./tests/Other/NewsletterTest');
const addtowishlistfromcatalogT = require('./tests/Wishlist/addToWhislistFromCatalog');
const plusMinusQtyT = require('./tests/Products/plusMinusQty');
const brandsFilterT = require('./tests/Filters/brandsFilter');
const priceFilterT = require('./tests/Filters/priceFilter');
const addtowishlistfromproductT = require('./tests/Wishlist/addToWhislistFromProduct');
const sizeFilterT = require('./tests/Filters/sizeFilter');
const passwordResetT = require('./tests/User/passwordChange');
const instantSearchT = require('./tests/Search/instantSearch');
const searchT = require('./tests/Search/searchTest');
const addRemoveCartT = require('./tests/Products/addRemoveCart');
const sizeUrlOptionT = require('./tests/Other/sizeUrlOptions');

module.exports = {
	loginTest : async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Login';
		await console.log(name + ' test started');
		await loginT.login(url, product, selectors, request, chromeLaunch);
	},
	chekoutTest : async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Checkout';
		await console.log(name + ' test started');
		await chekoutT.chekout(url, product, selectors, request, chromeLaunch);
	},
	sizeChartTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Size Chart';
		await console.log(name + ' test started');
		await sizechartT.sizechart(url, product, selectors, request, chromeLaunch);
	},
	sizeUrlOptionTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Size Url';
		await console.log(name + ' test started');
		await sizeUrlOptionT.sizeUrl(url, product, selectors, request, chromeLaunch);
	},
	registrationTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Registration';
		await console.log(name + ' test started');
		await registrationT.registration(url, product, selectors, request, chromeLaunch);
	},
	newsletterTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Newsletter Subscribe';
		await console.log(name + ' test started');
		await newsletterT.newsletter(url, product, selectors, request, chromeLaunch);
	},
	addToWishlistFromCatalogTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Add To Wishlist From Catalog';
		await console.log(name + ' test started');
		await addtowishlistfromcatalogT.atwfc(url, product, selectors, request, chromeLaunch);
	},
	plusMinusQtyTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Change Product Quantity In Cart';
		await console.log(name + ' test started');
		await plusMinusQtyT.plusminusqty(url, product, selectors, request, chromeLaunch);
	},
	brandsFilterTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Brands Filter';
		await console.log(name + ' test started');
		await brandsFilterT.brandsfilter(url, product, selectors, request, chromeLaunch);
	},
	priceFilterTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Price Filter';
		await console.log(name + ' test started');
		await priceFilterT.pricefilter(url, product, selectors, request, chromeLaunch);
	},
	addToWishlistFromProductTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Add To Wishlist From Product';
		await console.log(name + ' test started');
		await addtowishlistfromproductT.atwfp(url, product, selectors, request, chromeLaunch);
	},
	sizeFilterTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Size Filter';
		await console.log(name + ' test started');
		await sizeFilterT.sizefilter(url, product, selectors, request, chromeLaunch);
	},
	passwordResetTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Password Reset';
		await console.log(name + ' test started');
		await passwordResetT.passwordreset(url, product, selectors, request, chromeLaunch);
	},
	instantSearchTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Instant Search';
		await console.log(name + ' test started');
		await instantSearchT.instantsearch(url, product, selectors, request, chromeLaunch);
	},
	searchTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Search';
		await console.log(name + ' test started');
		await searchT.search(url, product, selectors, request, chromeLaunch);
	},
	addRemoveCartTest: async function(url, product, selectors, request, chromeLaunch){
		const name = await 'Add And Remove Product From Cart';
		await console.log(name + ' test started');
		await addRemoveCartT.addremovecart(url, product, selectors, request, chromeLaunch);
	}
};
