const tests = require('./tests');
let fs = require('fs');
const path = require('path');
const process = require('process');
let mainConfig = JSON.parse(fs.readFileSync('./config/config.json').toString());
let products = JSON.parse(fs.readFileSync('./config/products.json').toString());
// to run script write:
// npm test $test_url $test_env $test_name $headless

try {
	let devConfig = JSON.parse(fs.readFileSync('./config/config.dev.json').toString());
	let stagingConfig = JSON.parse(fs.readFileSync('./config/config.staging.json').toString());
	let prodConfig = JSON.parse(fs.readFileSync('./config/config.prod.json').toString());
	let productionCheck = false,
		name,
		url,
		chromeLaunch = mainConfig.headless,
		product,
		devCheck = false,
		requestUrl,
		selectors;
	let arguments = process.argv.slice(2);

	if(arguments[3] === 'false')
		chromeLaunch = false;
	else
		chromeLaunch = mainConfig.headless;

	if (arguments[0] === 'intersport') {
		if (arguments[1] === mainConfig.env.production) {
			productionCheck = true;
			url = prodConfig.url.intersport;
			product = products.intersportProduction;
			selectors = prodConfig.intersport;
			requestUrl = 'https://www.intersport.no/banner/ajax/load/?sections=';
		} else if (arguments[1] === mainConfig.env.staging) {
			url = stagingConfig.url.intersport;
			product = products.intersportStaging;
			selectors = stagingConfig.intersport;
			requestUrl = 'https://m2stagingintersport.gresvigdev.com/banner/ajax/load/?sections=';
		} else {
			devCheck = true;
			url = devConfig.url.intersport;
			product = products.intersportDev;
			selectors = devConfig.intersport;
			requestUrl = 'https://m2devintersport.gresvigdev.com/banner/ajax/load/?sections=';
		}
	}  else {
		if (arguments[1] === mainConfig.env.dev) {
			url = devConfig.url.gsport;
			product = products.gsportDev;
			selectors = devConfig.gsport;
			requestUrl = 'https://www.gsport.no/banner/ajax/load/?sections=';
		} else if (arguments[1] === mainConfig.env.staging) {
			url = stagingConfig.url.gsport;
			product = products.gsportStaging;
			selectors = stagingConfig.gsport;
			requestUrl = 'https://m2staging.gresvigdev.com/banner/ajax/load/?sections=';
		} else {
			url = prodConfig.url.gsport;
			product = products.gsportProduction;
			productionCheck = true;
			selectors = prodConfig.gsport;
			requestUrl = 'https://www.gsport.no/banner/ajax/load/?sections=';
		}
	}

	name = arguments[2];

    if (name)
    	t();

    async function t() {
        try {
            switch (name) {
                case 'login': {
                    await tests.loginTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'checkout': {
                    await tests.chekoutTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'sizeChart': {
                    await tests.sizeChartTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
				case 'sizeUrlOptionTest': {
					await tests.sizeUrlOptionTest(url, product, selectors, requestUrl, chromeLaunch);
					break;
				}
                case 'registration': {
                    await tests.registrationTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'newsletterSubscribe': {
                    await tests.newsletterTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'addToWishlistFromCatalog': {
                    await tests.addToWishlistFromCatalogTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'changeProductQuantityInCart': {
                    await tests.plusMinusQtyTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'brandsFilter': {
                    await tests.brandsFilterTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'priceFilter': {
                    await tests.priceFilterTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'addToWishlistFromProduct': {
                    await tests.addToWishlistFromProductTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'sizeFilter': {
                    await tests.sizeFilterTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'passwordReset': {
                    await tests.passwordResetTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'instantSearch': {
                    await tests.instantSearchTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'search': {
                    await tests.searchTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'addAndRemoveProductFromCart': {
                    await tests.addRemoveCartTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                case 'all': {
                    await tests.loginTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.chekoutTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.sizeChartTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.registrationTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.newsletterTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.addToWishlistFromCatalogTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.plusMinusQtyTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.brandsFilterTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.priceFilterTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.addToWishlistFromProductTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.sizeFilterTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.passwordResetTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.instantSearchTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.searchTest(url, product, selectors, requestUrl, chromeLaunch);
                    await tests.addRemoveCartTest(url, product, selectors, requestUrl, chromeLaunch);
                    break;
                }
                default : {
                    await console.log('something went wrong!');
                    break;
                }
            }
        } catch (err) {
            await console.log(err)
        }
    }
} catch (err) {
    console.log(err);
}
