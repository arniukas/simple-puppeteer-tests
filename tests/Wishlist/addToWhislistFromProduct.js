const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let addToWishlistFromProductTest = {
	testing : [],
	rand : gF.getRandomNumber(5),
	disabled : false,

	atwfp: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + 'customer/account/login/');
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET');
				await page.waitFor(selectors.loginForm.main);
				await page.type(selectors.loginForm.emailInput, 'test6218289@test.com');
				await page.type(selectors.loginForm.passwordInput, 'Asdasd123');
				await page.click(selectors.loginForm.submitButton);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then(() => this.testing.push("Logged successful"));
				await page.waitFor(3000);
				await page.waitForSelector(selectors.navbarSelectors[0].sko, {timeout : 3000});
				await page.click(selectors.navbarSelectors[0].sko);
				await page.click(selectors.navbarSelectors[0].sko);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then(() => this.testing.push("Product list founded"));
				await page.waitFor(500);
				await page.click(selectors.productLink[0].productLinkButton);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then( () => this.testing.push("Product loaded"));
				await page.waitFor(500);
				await page.waitForSelector(selectors.wishlistActions[0].addToWishlistFromProductButton, {timeout : 3000})
					.then(() => this.testing.push("Add to wishlist button founded"))
					.catch( async () => {
						this.disabled = true;
						await console.log('Wishlist doesn\'t exist');
						await this.testing.push("Wishlist doesn't exist.");
					});
				await page.click(selectors.wishlistActions[0].addToWishlistFromProductButton);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET');
				await page.waitFor(7000);
				await page.waitForSelector('.product-image-photo', { timeout : 5000 }).then( () => this.testing.push("Product founded in wishlist"));
				await page.waitForSelector(selectors.wishlistActions[0].removeToWishlistButton, {timeout : 5000});
				await page.click(selectors.wishlistActions[0].removeToWishlistButton);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then(() => this.testing.push("Product removed from wishlist successful"));
				await page.waitFor(500);
				await page.waitForSelector('.message.info.empty', {timeout : 3000});
				await console.log('Add To Wishlist from product test successful.');
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				if (!this.disabled)
					await console.log('Add To Wishlist from product test ' + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = addToWishlistFromProductTest;
