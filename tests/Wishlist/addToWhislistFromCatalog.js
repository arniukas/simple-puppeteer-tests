const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let addToWishlistFromCatalogTest = {
	testing : [],
	rand : gF.getRandomNumber(5),
	disabled : false,

	atwfc: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + 'customer/account/login/');
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200);
				await page.waitFor(selectors.loginForm.main);
				await page.type(selectors.loginForm.emailInput, 'test6218289@test.com');
				await page.type(selectors.loginForm.passwordInput, 'Asdasd123');
				await page.click(selectors.loginForm.submitButton);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then( () => this.testing.push("Logged successful"));
				await page.waitFor(3000);
				await page.click(selectors.navbarSelectors[0].sko);
				await page.click(selectors.navbarSelectors[0].sko);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET');
				await page.waitFor(3500);
				await page.waitForSelector(selectors.wishlistActions[0].addToWishlistFromCatalogButton, { timeout : 5000 })
					.then(() => this.testing.push("Add to wishlist button founded"))
					.catch( async () => {
						this.disabled = true;
						await console.log('Wishlist doesn\'t exist');
						await this.testing.push("Wishlist doesn't exist.");
					});
				await page.click(selectors.wishlistActions[0].addToWishlistFromCatalogButton);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET');
				await page.waitFor(7000);
				await page.waitForSelector('.product-image-photo', { timeout : 5000 }).then( () => this.testing.push("Product founded in wishlist"));
				await page.waitForSelector(selectors.wishlistActions[0].removeToWishlistButton, { timeout : 5000 });
				await page.click(selectors.wishlistActions[0].removeToWishlistButton);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then( () => this.testing.push("Product removed from wishlist successful"));
				await page.waitFor(500);
				await page.waitForSelector('.message.info.empty', { timeout : 5000 });
				await console.log('Add To Wishlist from Catalog test successful.');
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				await console.log('Add To Wishlist from Catalog test ' + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = addToWishlistFromCatalogTest;
