const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let checkoutTest = {
	testing : [],
	rand : gF.getRandomNumber(5),

	chekout : async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox']
			});
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + product);
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Product page loaded'));
				await page.waitFor(2000);
				await page.waitFor(selectors.addToCartButton).then( () => this.testing.push('Product added to cart'));
				await page.click(selectors.addToCartButton);
				await page.waitFor(2000);
				if (selectors.url === 'intersport') {
					if (url === 'https://m2stagingintersport.gresvigdev.com/')
						await page.click(selectors.checkoutButton);
					else
						await page.click(selectors.cartButton);
				}
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Checkout page loaded'));;
				await page.waitFor(6000);
				await page.click(selectors.checkoutForm.inputFields.paymentOption);
				await page.waitFor(3000);
				await page.type(selectors.checkoutForm.inputFields.postcode, '0580');
				await page.waitFor(4000);
				if (selectors.prod) {
					await page.waitForSelector(selectors.checkoutForm.inputFields.shippingMethod).then( () => this.testing.push('Payment option selected'));
					await page.click(selectors.checkoutForm.inputFields.shippingMethod);
					await page.waitFor(7000);
				} else if (selectors.dev) {
					await page.waitForSelector(selectors.checkoutForm.inputFields.paymentOption).then( () => this.testing.push('Payment option selected'));
					await page.click(selectors.checkoutForm.inputFields.paymentOption);
					await page.waitFor(7000);
				} else {
					await page.waitForSelector(selectors.checkoutForm.inputFields.paymentOption).then( () => this.testing.push('Payment option selected'));
					await page.click(selectors.checkoutForm.inputFields.paymentOption);
					await page.waitFor(7000);
				}
				await page.waitForSelector(selectors.checkoutForm.inputFields.customerEmail).then( () => this.testing.push('Email form founded'));
				await page.type(selectors.checkoutForm.inputFields.customerEmail, 'test@test.com');
				await page.waitFor(2000);
				if (url === 'https://m2staging.gresvigdev.com/' || url === 'https://www.gsport.no/')
					await page.click(selectors.checkoutForm.inputFields.telephone);
				await page.type(selectors.checkoutForm.inputFields.telephone, '94762014');
				await page.type(selectors.checkoutForm.inputFields.firstname, 'Testas');
				await page.type(selectors.checkoutForm.inputFields.lastname, 'Testutis');
				await page.waitFor(2000);
				if (selectors.url === 'intersport') {
					await page.type(selectors.checkoutForm.inputFields.city, 'Oslo');
					await page.type(selectors.checkoutForm.inputFields.street, 'Revierstredet 2');
					await page.waitFor(2000);
					await page.waitFor(selectors.submitCheckoutButton);
					await page.click(selectors.submitCheckoutButton);
				} else {
					await page.click('#checkout > div.opc-wrapper > div > div.checkout-sticky-wrap > div > div.totals_bottom > div.buttons_row > button');
				}
				await page.waitFor(15000);
				if (!selectors.dev)
					await page.waitForSelector('body > div > div > div.panel.panel-default > div > h4')
						.then( () => {
							this.testing.push('Billing page loaded');
							console.log('Checkout test successful');
						})
						.catch( () => {
							console.log('Checkout test failed');
							this.testing.push('Checkout doesn\'t redirect to payment page');
						});
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				await console.log('Checkout test ' + err);
			}
			await browser.close();
		})();

		return await this.testing;
	}
};



module.exports = checkoutTest;
