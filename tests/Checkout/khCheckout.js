const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let khChekoutTest = {
	testing : [],
	rand : gF.getRandomNumber(5),

	khChekout: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			try {
				await page.goto(url + product);
				// if (productionCheck) {
					await page.waitForResponse(response => response.url() === 'https://cdn.listagram.com/media/rotator_images/rotator_rotator_wheel_uHQEVHo.png' && response.status() === 200);
					await page.click(interSelec.cartButton[0]);
				// } else
				// 	await page.waitFor(10000);
				// const result = await page.evaluate(() => {
				// 	return document.querySelector('#attribute343').childNodes[1].value;
				// });
				// await page.select('#attribute343', result);
				await page.waitForSelector('#product_add_to_klikk_and_hent', { timeout : 1000});
				await page.click('#product_add_to_klikk_and_hent');
				await page.waitFor(4000);
				await page.click('#product_add_to_klikk_and_hent');
				await page.waitForSelector('.add_product_to_klikk_and_hent.action.primary', { timeout : 1000});
				await page.waitFor(2000);
				await page.click('.add_product_to_klikk_and_hent.action.primary');
				await page.waitFor(1000);
				await page.click('.action-close');
				await page.waitFor(2000);
				await page.waitForSelector('.action.showcart', { timeout : 1000});
				await page.click('.action.showcart');
				await page.waitFor(2000);
				await page.waitForSelector('#top-cart-btn-checkout', { timeout : 1000});
				await page.click('#top-cart-btn-checkout');
				await page.waitForNavigation();
				await page.waitFor(6000);
				await page.waitForSelector(interSelec.checkoutForm[0].customerEmail);
				await page.type(interSelec.checkoutForm[0].customerEmail, 'test@test.com');
				await page.type(interSelec.checkoutForm[0].telephone, '94762014');
				await page.type(interSelec.checkoutForm[0].firstname, 'Testas');
				await page.type(interSelec.checkoutForm[0].lastname, 'Testutis');
				await page.type(interSelec.checkoutForm[0].street, 'Revierstredet 2');
				await page.type(interSelec.checkoutForm[0].postcode, '0580');
				await page.type(interSelec.checkoutForm[0].city, 'Oslo');
				await page.waitFor(1000);
				await page.waitForSelector('.totals_bottom .action.primary.checkout', { timeout : 1000});
				await page.click('.totals_bottom .action.primary.checkout');
				await page.waitFor(1000);
				await page.click('.totals_bottom .action.primary.checkout');
				if (productionCheck) {
					await page.waitForNavigation();
				}
				await page.waitFor(4000);
				await console.log('Kh Chekout successful');
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				await console.log('Kh Chekout ' + err.message);
			}

			await browser.close();
		})();
	}

};


module.exports = khChekoutTest;

