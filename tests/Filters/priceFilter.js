const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let priceFilter = {
	rand : gF.getRandomNumber(5),
	testing : [],
	disabled : false,

	pricefilter: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 1000});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url);
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Page loaded successfuly'));
				await page.waitFor(interSelec.navbarSelectors[0].sko);
				await page.click(interSelec.navbarSelectors[0].sko);
				await page.click(interSelec.navbarSelectors[0].sko);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then( () => this.testing.push('Product page loaded'));
				await page.waitFor(1500);
				await page.waitFor('div[data-name="pris"]')
					.then( () => this.testing.push('Price filter founded'))
					.catch(async () => {
						this.disabled = true;
						await console.log("Price filter doesn't exist.");
						await this.testing.push("Price filter doesn't exist.");
					});
				await page.click('div[data-name="pris"]');
				await page.waitFor(8000);
				await page.click('#subcontent_container > ul > li:nth-child(1)');
				await page.waitFor(15000);
				let productPrice, productPriceHtml;
				productPrice = await page.$('.price-wrapper > .price');
				productPriceHtml = await page.evaluate(productPrice => productPrice.innerText.replace(/[.,-]/g, ''), productPrice);
				productPriceHtml = await parseInt(productPriceHtml);
				if (productPriceHtml <= 1000) {
					await console.log('Price filter test successful');
					await this.testing.push('Products filtered with price filter');
				} else {
					await console.log('Price filter test fail');
					await this.testing.push('Products not filtered with price filter');
				}
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				if (!this.disabled)
					await console.log('Price filter test ' + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}

};


module.exports = priceFilter;
