const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let sizeFilterTest = {
	rand : gF.getRandomNumber(5),
	testing : [],
	disabled : false,

	sizefilter: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + 'sko');
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Page loaded successfuly'));
				if (selectors.prod) {
					await page.waitFor(selectors.navbarSelectors[0].Barn.main);
					await page.click(selectors.navbarSelectors[0].Barn.main);
					await page.click(selectors.navbarSelectors[0].Barn.main);
				} else {
					await page.waitFor(selectors.navbarSelectors[0].sko);
					await page.click(selectors.navbarSelectors[0].sko);
					await page.click(selectors.navbarSelectors[0].sko);
				}
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then( () => this.testing.push('Product page loaded successfuly'));
				await page.waitFor(3000);
				await page.waitFor('.filter-item[data-name="størrelse"]', { timeout : 5000 })
					.then( () => this.testing.push('Size filter founded'))
					.catch(async () => {
						this.disabled = true;
						await this.testing.push('Size Filter disabled.');
						await console.log('Size Filter disabled.');
					});
				await page.click('.filter-item[data-name="størrelse"]');
				await page.waitFor(2000);
				let productCountBefore,productCountBeforeHtml;
				productCountBefore = await page.$('#product-count-total');
				productCountBeforeHtml = await page.evaluate(productCountBefore => productCountBefore.innerText, productCountBefore);
				productCountBeforeHtml = await parseInt(productCountBeforeHtml);
				await page.click('#subcontent_container > ul > li');
				await page.waitFor(5000);
				let productCountAfter,productCountAfterHtml;
				productCountAfter = await page.$('#product-count-total');
				productCountAfterHtml = await page.evaluate(productCountAfter => productCountAfter.innerText, productCountAfter);
				productCountAfterHtml = await parseInt(productCountAfterHtml);
				if (productCountBeforeHtml !== productCountAfterHtml) {
					await console.log('Size Filter test successful');
					await this.testing.push('Sizes filtered');
				} else {
					await this.testing.push('Sizes not filtered');
					await page.screenshot({path: directory+'/error'+this.rand+'.png'});
					await console.log('Size Filter test failed.');
				}
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				if (!this.disabled)
					await console.log('Size Filter test ' + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = sizeFilterTest;
