const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let BrandsFilterTest = {
	testing : [],
	rand : gF.getRandomNumber(5),
	disabled : false,

	brandsfilter: async function (url, product, selectors, requestUrl, chromeLaunch) {

		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url);
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push("Page loaded"));
				await page.waitFor(interSelec.navbarSelectors[0].sko);
				await page.click(interSelec.navbarSelectors[0].sko);
				await page.click(interSelec.navbarSelectors[0].sko);
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then(() => this.testing.push("Product page loaded"));
				await page.waitFor(1500);
				await page.waitFor(interSelec.filters[0].brandFilter)
					.then( () => this.testing.push("Brand filter founded"))
					.catch( () => {
						this.disabled = true;
						console.log('Brand filter doesn\'t exist');
						this.testing.push("Brand filter doesn't exist");
					});
				await page.click(interSelec.filters[0].brandFilter);
				await page.waitFor(2000);
				await page.click(interSelec.filters[0].brand);
				if (selectors.prod) {
					await page.waitFor(10000);
				} else
					await page.waitFor(12000);
				let productBrand,productBrandHtml, filterBrand, filterBrandHtml;
				productBrand = await page.$('.filter-current > .items > li.item span');
				productBrandHtml = await page.evaluate(productBrand => productBrand.innerText, productBrand);
				filterBrand = await page.$('.product-item-brand');
				filterBrandHtml = await page.evaluate(filterBrand => filterBrand.innerText, filterBrand);
				await page.waitFor(10000);
				if (filterBrandHtml === productBrandHtml) {
					this.resultOfTest = await 'Brands filter test successful';
					await this.testing.push('Brand ' + filterBrandHtml + ' = ' + productBrandHtml);
				} else {
					productBrandHtml = await productBrandHtml.toLowerCase();
					if (filterBrandHtml === productBrandHtml) {
						await console.log('Brands filter test successful');
						await this.testing.push('Brand ' + filterBrandHtml + ' = ' + productBrandHtml);
					} else
						await console.log('Brands filter test fail. Product Brand: ' + productBrandHtml +'!='+filterBrandHtml);
				}
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				if (!this.disabled)
					await console.log('Brands filter test ' + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = BrandsFilterTest;
