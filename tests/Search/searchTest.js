const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let searchTest = {
	rand : gF.getRandomNumber(5),
	testing : [],

	search: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + 'sko');
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Page loaded successfuly'));;
				if (selectors.prod) {
					await page.waitFor(selectors.navbarSelectors[0].Barn.main);
					await page.click(selectors.navbarSelectors[0].Barn.main);
					await page.click(selectors.navbarSelectors[0].Barn.main);
				} else {
					await page.waitFor(selectors.navbarSelectors[0].sko);
					await page.click(selectors.navbarSelectors[0].sko);
					await page.click(selectors.navbarSelectors[0].sko);
				}
				await page.waitForRequest(request => request.url() === requestUrl && request.method() === 'GET').then( () => this.testing.push('Product page loaded successfuly'));;
				await page.waitFor(500);
				let productList = await page.evaluate(() => {
					let el = document.querySelectorAll('.product-item-link');
					let list = [];
					for (let i = 0; i < 5;i++) {
						list[i] = el[i].innerText;
					}
					return list;
				});
				let elSearch = [];
				let elExist = [];
				await this.testing.push('Product names collected for search');
				for(let i = 0;i < 5;i++) {
					await page.type('#search_mini_form input', productList[i]);
					await page.waitFor(1000);
					await page.click('#algolia-glass');
					await page.waitForNavigation({ waitUntil: 'networkidle0' });
					elExist[i] = await page.$('.no-results');
					if (elExist[i]) {
						elSearch[i] = 'Product: ' + productList[i] + ' was not found';
					} else {
						elSearch[i] = 'Product: ' + productList[i] + ' founded';
					}

					this.testing.push(elSearch[i]);
				}
				await console.log('Search test successful')
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				await console.log('Search test ' + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = searchTest;
