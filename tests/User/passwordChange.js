const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let passwordChangeTest = {
	rand : gF.getRandomNumber(5),
	testing : [],

	passwordreset: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + 'customer/account/login/');
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Login page loaded'));
				await page.waitFor(selectors.loginForm.main).then( () => this.testing.push('Login form founded'));
				await page.type(selectors.loginForm.emailInput, 'test6218289@test.com');
				await page.type(selectors.loginForm.passwordInput, 'Asdasd123');
				await page.click(selectors.loginForm.submitButton);
				await page.waitFor(18000);
				await page.click(selectors.passwordChangeButton);
				for (let i = 0; i < 2; i++) {
					await page.waitFor(8000);
					if (url !== 'https://www.gsport.no/') {
						await page.waitForSelector('#form-validate > div.flex-col.left > div.address_box_block > div > div.box_wrap.contact_information > div > div.save-edit-button').then( () => this.testing.push('Password change form founded'));
						await page.click('#form-validate > div.flex-col.left > div.address_box_block > div > div.box_wrap.contact_information > div > div.save-edit-button');
						await page.waitFor(7000);
						if (i === 0) {
							await page.type('#password', 'Asdasd1234');
							await page.type('#password-confirmation', 'Asdasd1234');
							await page.type('#current-password', 'Asdasd123');
							await this.testing.push('Password changed to Asdasd1234');
						} else if (i === 1) {
							await page.click("#password", {clickCount: 3});
							await page.keyboard.press('Backspace');
							await page.type('#password', 'Asdasd123');
							await page.click("#password-confirmation", {clickCount: 3});
							await page.keyboard.press('Backspace');
							await page.type('#password-confirmation', 'Asdasd123');
							await page.click("#current-password", {clickCount: 3});
							await page.keyboard.press('Backspace');
							await page.type('#current-password', 'Asdasd1234');
							await this.testing.push('Password changed to Asdasd123');
						}
						await page.waitFor(1000);
						await page.click('#form-validate > div.flex-col.left > div.address_box_block > div > div.box_wrap.contact_information > div > .save-edit-button').then( () => this.testing.push('Password change form founded'));
						await page.waitFor(8000);
					} else {
						if (i === 0) {
							await page.click('#change-password');
							await page.waitFor(2000);
							await page.type('#password', 'Asdasd1234');
							await page.type('#password-confirmation', 'Asdasd1234');
							await page.type('#current-password', 'Asdasd123');
							await this.testing.push('Password changed to Asdasd1234');
						} else if (i === 1) {
							await page.click(selectors.passwordChangeButton);
							await page.waitFor(8000);
							await page.click('#change-password');
							await page.waitFor(2000);
							await page.type('#password', 'Asdasd123');
							await page.type('#password-confirmation', 'Asdasd123');
							await page.type('#current-password', 'Asdasd1234');
							await this.testing.push('Password changed to Asdasd123');
						}
						await page.waitFor(1000);
						await page.click('#form-validate > div > div.primary > button');
						await page.waitFor(8000);
					}
				}
				await page.waitFor(6000);
				await console.log('Password Reset test successful');
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				await console.log("Password Reset test " + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = passwordChangeTest;
