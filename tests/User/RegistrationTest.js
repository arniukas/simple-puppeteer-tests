const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let registrationTest = {
	testing : [],
	rand : gF.getRandomNumber(5),

	registration: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + 'customer/account/create/');
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Page loaded successfuly'));
				if (url !== 'https://www.gsport.no/')
					await page.click('#register-member-button');
				await page.waitFor(8000);
				await page.waitFor(selectors.regForm.main).then( () => this.testing.push('Registration form founded'));
				await page.type(selectors.regForm.phone, '954'+this.rand);
				await page.type(selectors.regForm.firstname, 'Meat');
				await page.type(selectors.regForm.lastname, 'Tester');
				if (url !== 'https://www.gsport.no/') {
					await page.type(selectors.regForm.street, 'Revierstredet 2');
					await page.type(selectors.regForm.zip, '0580');
					await page.type(selectors.regForm.city, 'Oslo');
				}
				await page.type(selectors.regForm.email, 'test'+this.rand+'@test.com');
				await page.type(selectors.regForm.password, 'Asdasd123');
				await page.type(selectors.regForm.passwordConfirm, 'Asdasd123');
				await page.waitFor(2000);
				await page.click(selectors.regForm.submitButton);
				await page.waitFor(3000);
				await page.waitForSelector(selectors.regForm.verification).then( () => this.testing.push('Registration form verify popup founded'));
				await console.log('Registration test successful');
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				await console.log("Registration test " + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}

};

module.exports = registrationTest;
