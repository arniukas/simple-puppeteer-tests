const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let loginTest = {
	rand : gF.getRandomNumber(5),
	testing : [],

	login: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + 'customer/account/login/');
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200);
				await page.waitFor(selectors.loginForm.main).then( () => this.testing.push('Login form was found'));
				await page.type(selectors.loginForm.emailInput, 'test6218289@test.com');
				await page.type(selectors.loginForm.passwordInput, 'Asdasd123');
				await page.click(selectors.loginForm.submitButton);
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Landing page loaded'));
				await page.waitFor(2000);
				await console.log('Login test successful');
			} catch (err) {
				await console.log('Login test ' + err.message);
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
			}
			await browser.close();
		})();

		return await this.testing;
	}

};

module.exports = loginTest;
