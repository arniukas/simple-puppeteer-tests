const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let addRemoveCartTest = {
	rand : gF.getRandomNumber(5),
	testing : [],

	addremovecart : async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox']
			});
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + product);
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200);
				await page.waitFor(2000);
				await page.waitForSelector(selectors.addToCartButton, { timeout : 5000}).then( () => this.testing.push("Product added to cart"));
				await page.click(selectors.addToCartButton);
				await page.waitFor(2000);
				if (selectors.url === 'intersport') {
					if (url === 'https://m2stagingintersport.gresvigdev.com/')
						await page.click(selectors.checkoutButton);
					else
						await page.click(selectors.cartButton);
				}
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200);
				await page.waitFor(3000);
				await page.waitForSelector(selectors.removeFromCartButton, { timeout : 5000}).then( () => this.testing.push("Checkout page loaded"));
				await page.click(selectors.removeFromCartButton);
				await page.waitFor(3000);
				await page.waitForSelector(selectors.modalAcceptButton, { timeout : 5000}).then( () => this.testing.push("Product removed from cart"));
				await page.click(selectors.modalAcceptButton);
				await page.waitFor(4000);
				await page.waitForSelector('.base', { timeout : 5000}).then( () => this.resultOfTest = 'Add and Remove product from cart Test successful');
				await console.log("Add and Remove product from cart Test successful");
			} catch (err) {
				await console.log('Add and Remove product from cart test ' + err.message);
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
			}
			await browser.close();
		})();

		return await this.testing;
	}

};

module.exports = addRemoveCartTest;
