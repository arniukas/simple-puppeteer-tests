const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let plusMinusQty = {
	rand : gF.getRandomNumber(5),
	testing : [],

	plusminusqty: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + product);
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Page loaded successfuly'));
				await page.waitFor(2000);
				await page.waitFor(selectors.addToCartButton).then( () => this.testing.push('Cart button founded'));
				await page.click(selectors.addToCartButton);
				await page.waitFor(2000);
				if (selectors.url === 'intersport') {
					if (url === 'https://m2stagingintersport.gresvigdev.com/')
						await page.click(selectors.checkoutButton);
					else
						await page.click(selectors.cartButton);
				}
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Checkout page loaded successfuly'));
				await page.waitFor(6000);
				let priceBeforeClick,priceBeforeClickHtml, priceAfterClick, priceAfterClickHtml;
				priceBeforeClick = await page.$('span.price:nth-child(1)');
				priceBeforeClickHtml = await page.evaluate(body => body.innerText, priceBeforeClick);
				await page.click('span.qty-btn:nth-child(3)');
				if (url === 'https://m2stagingintersport.gresvigdev.com/')
					await page.waitFor(15000);
				else
					await page.waitFor(8000);
				priceAfterClick = await page.$('span.price:nth-child(1)');
				priceAfterClickHtml = await page.evaluate(body => body.innerText, priceAfterClick);
				if (priceAfterClickHtml !== priceBeforeClickHtml) {
					await this.testing.push('Price changed after quantity increase');
					await console.log('Quantity change test was successful');
				} else {
					await this.testing.push('Price does not changed after quantity increase');
					await console.log('Quantity change test was unsuccessful');
				}
			} catch (err) {
				await console.log("Change product quantity test " + err.message);
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
			}
			await page.waitFor(1000);
			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = plusMinusQty;
