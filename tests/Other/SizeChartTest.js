const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let SizeChartTest = {
	rand : gF.getRandomNumber(5),
	testing : [],
	disabled : false,

	sizechart: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + product);
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Page loaded successfuly'));
				await page.waitFor('#tab-label-meat_sizeguide-title', { timeout : 5000})
					.then( () => this.testing.push('Size chart founded'))
					.catch(async () => {
						this.disabled = true;
						await this.testing.push("Size Chart doesn't exist.");
						await console.log('Size Chart doesn\'t exist.');
					});
				await page.click('#tab-label-meat_sizeguide-title');
				await page.waitFor(5000);
				let listLength = async function () {
					let list = await document.querySelector(".bluefoot-accordion.bluefoot-full-width.bluefoot-entity");
					return await list.children.length;
				};
				if (listLength) {
					await console.log('Size Chart test successful');
				} else {
					await page.screenshot({path: directory+'/error'+this.rand+'.png'});
					await console.log('Size Chart test failed.');
					await this.testing.push('Size chart not works');
				}
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				if (!this.disabled)
					await console.log('Size Chart test ' + err.message);
			}

			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = SizeChartTest;
