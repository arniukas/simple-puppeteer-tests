const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let sizeUrlOptionTest = {
	rand : gF.getRandomNumber(5),
	testing : [],

	sizeUrl: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url + 'sykkel');
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200);
				await page.waitForSelector('li.product:nth-child(4) > div:nth-child(1) > div:nth-child(3) > div:nth-child(4) > span:nth-child(1) > a:nth-child(2)');
				await page.click('li.product:nth-child(4) > div:nth-child(1) > div:nth-child(3) > div:nth-child(4) > span:nth-child(1) > a:nth-child(2)');
				await page.waitForSelector('.swatch-select', { timeout : 2000 });
				await this.testing.push('success');
			} catch (err) {
				await console.log('Size Url test ' + err.message);
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
			}
			await browser.close();
		})();

		await console.log(this.testing);
		return await this.testing;
	}

};

module.exports = sizeUrlOptionTest;
