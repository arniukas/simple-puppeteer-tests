const path = require('path');
let projectUrl = path.dirname(require.main.filename);
const puppeteer = require('puppeteer');
let gF = require(projectUrl+'/globalFunctions');
const directory = projectUrl + '/errors';

let newsletterTest = {
	rand : gF.getRandomNumber(6),
	testing : [],
	disabled : false,

	newsletter: async function (url, product, selectors, requestUrl, chromeLaunch) {
		await (async () => {
			const browser = await puppeteer.launch({
				headless: chromeLaunch,
				args: ['--no-sandbox'] });
			const page = await browser.newPage();
			await page.setViewport({width: 1200, height: 800});
			await page.setRequestInterception(true);
			page.on('request', (req) => {
				if(req.resourceType() === 'image' || req.url() === 'https://cdn.listagram.com/static/api/listagram.js')
					req.abort();
				else
					req.continue();
			});
			try {
				await page.goto(url);
				await page.waitForResponse(response => response.url() === requestUrl && response.status() === 200).then( () => this.testing.push('Page loaded'));;
				await page.waitFor(2000);
				await page.waitFor('#newsletter-validate-detail').then( () => this.testing.push('Newsletter Subscribe form founded'));
				await page.type('#newsletter', 'test'+this.rand+'@test.com');
				await page.click(selectors.newsletterButton);
				await page.waitFor(6000);
				await page.waitForSelector('.message-success.success.message')
					.then( () => {
						this.testing.push('Newsletter Subscribe test successful');
						console.log('Newsletter Subscribe test successful');
					})
					.catch( (err) => {
						this.disabled = true;
						this.testing.push('Newsletter Subscribe test failed' + err.message);
						console.log('Newsletter Subscribe test unsuccessful');
					});
			} catch (err) {
				await page.screenshot({path: directory+'/error'+this.rand+'.png'});
				if (!this.disabled)
					await console.log("Newsletter Subscribe test " + err.message);
			}
			await browser.close();
		})();

		return await this.testing;
	}
};


module.exports = newsletterTest;
