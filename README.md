# Simple Puppeteer Tests

Puppeteer tests for testing Gsport and Intersport websites

## Getting Started

 Clone repository

 Open console in project folder and install npm packages
```
    npm install
```

 Now run tests with simple command
 
```
    npm test [gsport/intersport] [test_enviroment] [test_name] [show_chrome=true/false]
    npm test intersport production login false
```
 
 By Default Chrome launch is set to false and it opens Chrome browser.
 Set Chrome_show to true if don't want to launch Chrome browser.
 
## Commands to launch test by name in production
 
```
    npm test intersport production login false
    npm test intersport production khChekout false
    npm test intersport production checkout false
    npm test intersport production sizeChart false
    npm test intersport production registration false
    npm test intersport production newsletterSubscribe false
    npm test intersport production addToWishlistFromCatalog false
    npm test intersport production changeProductQuantityInCart false
    npm test intersport production brandsFilter false
    npm test intersport production priceFilter false
    npm test intersport production addToWishlistFromProduct false
    npm test intersport production sizeFilter false
    npm test intersport production passwordReset false
    npm test intersport production instantSearch false
    npm test intersport production search false
    npm test intersport production addAndRemoveProductFromCart false
    npm test intersport production registerToKundeKlubb false
```
 To run all tests write: 
```
    npm test intersport production all false
```
